const data = [{
    "id": 1,
    "question": {
        "id": 5,
        "poll_id": 1,
        "label": "¿What is your favorite Framework?"
    },
    "answers": [{
            "id": 6,
            "question_id": 1,
            "label": "Angular",
            "poll_id": 1
        },
        {
            "id": 7,
            "question_id": 2,
            "label": "Laravel",
            "poll_id": 1
        },
        {
            "id": 8,
            "question_id": 3,
            "label": "KrakenJS",
            "poll_id": 1
        }
    ],
    "votes": [{
        "id": 7,
        "user_id": 3,
        "answer_id": 4,
        "poll_id": 5
    }]
}];

module.exports = data;