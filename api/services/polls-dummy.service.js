const preloaderPolls = require('./../data/polls');
const _ = require('lodash');

// Singleton
let instance = null;

class PollsDummyService {
    constructor () {
        if(! instance) this.init();
        return instance;
    }

    init() {
        instance = this;
        this.polls = preloaderPolls;
    }

    save (poll) {
        this.polls.push(poll);
        return poll;
    }

    getAll() {
        return this.polls;
    }

    getById( id ) {
        const poll = _.find(this.polls, { id: parseInt(id, 10 ) } );
        return poll ? poll : false;
    }

    transformAnswerToVote(answer) {
        const poll = _.find(this.polls, { id: parseInt(answer.poll_id, 10)});

        return {
            id: poll.votes.length + 1,
            user_id: answer.user_id,
            question: answer.question_id,
            poll_id: answer.poll_id
        };
    }

    saveVoteToPoll(vote) {
        const poll = _.find(this.polls, {id: parseInt(vote.poll_id, 10)});
        poll.votes.push(vote);
    }
}

module.exports = new PollsDummyService();