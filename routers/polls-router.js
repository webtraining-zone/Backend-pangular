const pollsRouter = require('express').Router();
const pollsService = require('./../api/services/polls-dummy.service');


// polls
pollsRouter.get('/', (request, response) => {
    response.json( pollsService.getAll() );
} );

pollsRouter.param('id', (request, response, next, id) => {
    console.log('ID Recibido', id);
    const idToFind = + id;
    const poll = pollsService.getById(idToFind);

    if(poll) {
        request.poll = poll;
        next();
    }else {
        response.json({'Error': 'ID Not Found'});
    }
});

pollsRouter.get('/:id', (request, response) => {
    const poll = request.poll;
    response.json(poll || {'error': 'poll Not Found'});
});


pollsRouter.post('/', (request, response) => {
    const poll = request.body;
    const createdPoll = pollsService.save(poll);

    if (!createdPoll) response.json({'error': 'Poll was not created'});

    response.status( 201 ).json( createdPoll );
});


// Error handler
pollsRouter.use((error, request, response, next) => {
    if(error) {
        response.status(500).send(error);
    }
} );

module.exports = pollsRouter;