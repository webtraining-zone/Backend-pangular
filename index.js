const express = require('express');
const app = express();
const path = require('path');
const server = require('http').createServer(app);
const pollsRouter = require('./routers/polls-router');
const allowCrossOrigin = require('./api/middlewares/cors.middleware');
const pollService = require('./api/services/polls-dummy.service');

const morgan = require('morgan');
const bodyParser = require('body-parser');
const port  = 3300;


const io = require('socket.io')(server);


app.use(morgan('dev'));

app.use(allowCrossOrigin);

app.use( bodyParser.urlencoded( { extended: true } ) );

app.use( bodyParser.json() );


app.get('/', (request, response) => {
    response.sendFile(path.join(__dirname, 'index.html') );
});

app.use('/api/v1/polls', pollsRouter);




// Socket.io

io.on('connection', (socket) => {
    console.log('User connected');

    socket.on('new-vote', (answer) => {
        console.log('Answer was received:', answer);

        // Había un error de nombre del método "transformAnswerToVote"
        const vote = pollService.transformAnswerToVote(answer);
        pollService.saveVoteToPoll(vote);


        // Nuevas líneas para continuar con el ejercicio
        const poll = pollService.getById(vote.poll_id);

        io.emit('vote-received', {
            poll: poll
        });

    });

    socket.on('disconnect', () => {
        console.log('User disconnected');


    });
});

//




server.listen( port, () => {
    console.log('Servidor ejecutandose en puerto', port);
});